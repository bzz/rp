//
//  ProjectNamePanel.m
//  RP
//
//  Created by Mikhail Baynov on 19/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "ProjectNameVM.h"
#import "Model.h"
#import "ProjectCell.h"
#import "NSNotificationCenter+addOn.h"


@interface ProjectNameVM ()
{
    Model *model;
    NSNotificationCenter *notificationCenter;
    UIImage *emptyImage;
}
@end


@implementation ProjectNameVM
- (instancetype)init {
    NSLog(@"USE INIT WITH PANEL, NOT INIT!");
    return nil;
}


- (instancetype)initWithPanel:(Panel *)panel {
    if (self = [super initWithPanel:panel]) {
        model = [Model sharedInstance];
        notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self notifier:NotifierReloadViews];
        emptyImage = [UIImage imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(40, 40)];  //testtest
        
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button addTarget:self action:@selector(newProjectButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_button setTitle:@"New" forState:UIControlStateNormal];
        _button.frame = CGRectMake(0, 0, self.width/5, self.height);
        _button.backgroundColor = [UIColor hex:0x588FC5];
        [self.view addSubview:_button];
        
        UICollectionViewFlowLayout *l=[[UICollectionViewFlowLayout alloc] init];
        l.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
        l.minimumInteritemSpacing = 2;
        l.minimumLineSpacing = 5;
        l.itemSize = CGSizeMake(self.width*4/5 - 10, (self.width*4/5-10)/3);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(self.height/5, 0, self.width*4/5, self.height) collectionViewLayout:l];
        
        
        [_collectionView registerClass:[ProjectCell class] forCellWithReuseIdentifier:@"Cell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_collectionView];
        
        [self formProjectNamesArray];
        
        
    }
    return self;
}





- (void)newProjectButtonPressed {
    [self createNewFolder];
    model.picture = emptyImage;
    [self formProjectNamesArray];
    [self.collectionView reloadData];
    [self.collectionView setContentOffset:CGPointMake(0, self.collectionView.contentSize.height - self.collectionView.frame.size.height + 150) animated:YES];
//    [self performSegueWithIdentifier:@"toFileManager" sender:self];
}
- (void)createNewFolder {
    NSInteger i = 1;
    NSString *projectName = [NSString stringWithFormat:@"Project%02i",i];
    NSString *dataPath = [model.documentsDirectory stringByAppendingPathComponent:projectName];
    while ([model.fileManager fileExistsAtPath:dataPath]) {
        i++;
        projectName = [NSString stringWithFormat:@"Project%02i",i];
        dataPath = [model.documentsDirectory stringByAppendingPathComponent:projectName];
    }
    [model.fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    model.filesDirectory = dataPath;
    [[NSNotificationCenter defaultCenter] postNotifier:NotifierReloadViews];
}




- (void)formProjectNamesArray {
    if (self.projectNamesArray) {
        [self.projectNamesArray removeAllObjects];
    } else 	{
        self.projectNamesArray = [NSMutableArray new];
    }
    [self addProject:@"/" image:emptyImage];
    for (NSString *file in [model.fileManager contentsOfDirectoryAtPath:model.documentsDirectory error:nil]) {
        BOOL isDirectory = NO;
        if ([model.fileManager fileExistsAtPath:[model.documentsDirectory stringByAppendingPathComponent:file] isDirectory:&isDirectory]) {
            if (isDirectory) {
                [self addProject:file image:emptyImage];
            }
        }
    }
}

- (void)addProject:(NSString*)name image:(UIImage*)image {
    [self.projectNamesArray addObject:name];
}




#pragma mark
#pragma mark Delegates
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProjectCell *cell = (ProjectCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.inputField.label.text = self.projectNamesArray[indexPath.row];
    NSInteger filesCount = [self countFilesAtPath:[model.documentsDirectory stringByAppendingPathComponent:cell.inputField.label.text]];
    cell.filesCountLabel.text = filesCount == 1? @"1 file" :[NSString stringWithFormat:@"%li files", (long)filesCount];
    
    cell.deleteButton.tag = indexPath.row;
    
    if ([cell.inputField.label.text isEqualToString:@"/"]) {
        cell.deleteButton.hidden = YES;
        cell.filesCountLabel.text = nil;
    }
    
    return cell;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.projectNamesArray.count;
}

- (NSInteger)countFilesAtPath:(NSString*)path {
    NSArray *array = [model.fileManager contentsOfDirectoryAtPath:path error:nil];
    NSInteger count = array.count;
    for (path in array) {
        BOOL isDirectory = NO;
        if ([model.fileManager fileExistsAtPath:[model.documentsDirectory stringByAppendingPathComponent:path] isDirectory:&isDirectory]) {
            if (isDirectory) {
                count--;
            }
        }
    }
    return count;
}


#pragma mark
#pragma mark
#pragma mark ACTIONS

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ProjectCell *cell = (ProjectCell*)[collectionView cellForItemAtIndexPath:indexPath];
    model.filesDirectory = [model.documentsDirectory stringByAppendingPathComponent:cell.inputField.label.text];
    [[NSNotificationCenter defaultCenter] postNotifier:NotifierReloadViews];
    [self.collectionView reloadData];
}


- (void)notified {
    [self formProjectNamesArray];
    [self.collectionView reloadData];
}
- (void)dealloc
{
    [notificationCenter removeObserver:self];
}


@end
