//
//  Button.m
//  RP
//
//  Created by Mikhail Baynov on 19/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "ControlButton.h"


@interface ControlButton ()
{
UIImageView* _imageView;
}
@end


@implementation ControlButton

- (instancetype)init {
    if (self=[super init]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self=[super initWithCoder:aDecoder]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self=[super initWithFrame:frame]) {
        self.frame = frame;
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
    _imageView = [[UIImageView alloc]initWithFrame:self.bounds];
    [self addSubview:_imageView];
}



#pragma mark - SETTERS
- (void)setImage:(UIImage *)image {
    _image = image;
    if (!_imageSelected) {
        _imageSelected = image;
    }
    [self redraw];
}
- (void)setImageSelected:(UIImage *)imageSelected {
    _imageSelected = imageSelected;
    [self redraw];
}
- (void)setSelected:(BOOL)selected {
    _selected = selected;
    [self redraw];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    _imageView.frame = self.bounds;
    [self redraw];
}



#pragma mark - 

- (void)redraw {
    _imageView.image = _selected? _imageSelected : _image;
}


@end
