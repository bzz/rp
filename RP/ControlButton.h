//
//  Button.h
//  RP
//
//  Created by Mikhail Baynov on 19/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ControlButton : UIView

@property (strong, nonatomic)UIImage *image;
@property (strong, nonatomic)UIImage *imageSelected;


@property (nonatomic) BOOL selected;

@end
