//
//  PreviewVM.m
//  RP
//
//  Created by Mikhail Baynov on 27/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "PreviewVM.h"
#import "NSNotificationCenter+addOn.h"



@interface PreviewVM ()
{
    Model *model;
    NSNotificationCenter *notificationCenter;
    CGFloat coeff;
}
@end


@implementation PreviewVM
- (instancetype)init {
    NSLog(@"USE INIT WITH PANEL, NOT INIT!");
    return nil;
}


- (instancetype)initWithPanel:(Panel *)panel {
    if (self = [super initWithPanel:panel]) {
        model = [Model sharedInstance];
        notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self notifier:NotifierReloadViews];
        
        _pictureView = [[UIImageView alloc]initWithFrame:self.view.bounds];
        _areaView = [[UIView alloc]init];

        [self.view addSubview:_pictureView];
        
        [self notified];
    }
    return self;
}

- (void)notified {
    self.pictureView.image = model.picture;
    
    UIImage *tmp = [UIImage imageWithContentsOfFile:model.fileNameFullPath];
    if (tmp) {
        model.picture = tmp;
        self.pictureView.image = tmp;
    }
    
    [self setupAreaView];
    [self formPictureArea];
    
    CGRect frame = [self getFrameSizeForImage:self.pictureView.image inImageView:self.view];
    self.pictureView.frame = CGRectMake(self.view.frame.origin.x + frame.origin.x, self.view.frame.origin.y + frame.origin.y, frame.size.width, frame.size.height);
    [self validateAreaViewFrame];
}

#pragma mark -
#pragma mark - LOAD PICTURE FROM FILE



- (void)setupAreaView
{
    self.areaView.backgroundColor = [UIColor clearColor];
    self.areaView.layer.borderWidth = 2.0;
    self.areaView.layer.borderColor = [UIColor purpleColor].CGColor;
    [self.pictureView addSubview:self.areaView];
}











- (CGRect)getFrameSizeForImage:(UIImage *)image inImageView:(UIView *)imageView
{
    CGFloat hfactor = image.size.width / imageView.frame.size.width;
    CGFloat vfactor = image.size.height / imageView.frame.size.height;
    CGFloat factor = fmax(hfactor, vfactor);
    CGFloat newWidth = image.size.width / factor;
    CGFloat newHeight = image.size.height / factor;
    CGFloat leftOffset = (imageView.frame.size.width - newWidth) / 2;
    CGFloat topOffset = (imageView.frame.size.height - newHeight) / 2;
    return CGRectMake(leftOffset, topOffset, newWidth, newHeight);
}




#pragma mark -
#pragma mark - touches

- (void)touchBegan:(CGPoint)point {
    self.areaView.center = point;
    [self validateAreaViewFrame];
}
- (void)touchMoved:(CGPoint)point {
    [self touchBegan:point];
}
- (void)touchEnded:(CGPoint)point {
    [self formPictureArea];
}


- (void)validateAreaViewFrame
{
    CGFloat origX = self.areaView.frame.origin.x;
    CGFloat origY = self.areaView.frame.origin.y;
    coeff = MIN(self.pictureView.frame.size.width/model.picture.size.width, self.pictureView.frame.size.height/model.picture.size.height);
    CGFloat w = (model.picture.size.width <= 340*coeff)?  340*coeff : _areaView.frame.size.width;
    CGFloat h = (model.picture.size.height <= 340*coeff)? 340*coeff : _areaView.frame.size.height;
    h = MIN(h, model.picture.size.height*coeff);
    w = MIN(w, model.picture.size.width*coeff);
    self.areaView.frame = CGRectMake(origX, origY, w, h);

    CGFloat mwidth = self.pictureView.frame.size.width - w;
    CGFloat mheight = self.pictureView.frame.size.height - h;
    origX = (origX < 0)? 0 : (origX > mwidth)?  mwidth  : origX;
    origY = (origY < 0)? 0 : (origY > mheight)? mheight : origY;
    self.areaView.frame = CGRectMake(origX, origY, w, h);
}

- (void)formPictureArea
{
    model.pictureAreaRect = CGRectMake((int)self.areaView.frame.origin.x / coeff, (int)self.areaView.frame.origin.y /coeff,
                                       (int)self.areaView.frame.size.width /coeff, (int)self.areaView.frame.size.height /coeff);
    if (model.picture.size.width <=340 && model.picture.size.height <=340) {
        model.pictureArea = model.picture;
    } else {
        model.pictureArea = [UIImage imageWithCGImage:CGImageCreateWithImageInRect(model.picture.CGImage, model.pictureAreaRect)];
    }
    model.pictureAreaData = [NSMutableData dataFromImage:model.pictureArea];
}


- (void)dealloc {
    [notificationCenter removeObserver:self];
}


@end
