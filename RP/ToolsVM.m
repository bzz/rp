//
//  ToolsPanel.m
//  RP
//
//  Created by Mikhail Baynov on 19/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "ToolsVM.h"
#import "Model.h"
#import "NSNotificationCenter+addOn.h"

@interface ToolsVM ()
{
    Model *model;
    NSNotificationCenter *notificationCenter;
}
@end

enum {COLLECTION_PALETTE=101, COLLECTION_TOOL=102};



@implementation ToolsVM



- (instancetype)initWithPanel:(Panel *)panel {
    if (self = [super initWithPanel:panel]) {
        model = [Model sharedInstance];
        notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self notifier:NotifierReloadViews];
        
        _colorView = [[UIView alloc]initWithFrame:CGRectMake(self.width/2, 0, self.width/2, self.height/5)];
        _colorView.backgroundColor = model.selectedColor;
        [self.view addSubview:_colorView];
        
        

        UICollectionViewFlowLayout *l1=[[UICollectionViewFlowLayout alloc] init];
        l1.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
        l1.minimumInteritemSpacing = 2;
        l1.minimumLineSpacing = 5;
        _toolsCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.width/2, self.height) collectionViewLayout:l1];
        [_toolsCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"toolCell"];
        _toolsCollectionView.backgroundColor = [UIColor clearColor];
        _toolsCollectionView.delegate = self;
        _toolsCollectionView.dataSource = self;
        [self.view addSubview:_toolsCollectionView];

        
        
        UICollectionViewFlowLayout *l2=[[UICollectionViewFlowLayout alloc] init];
        l2.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
        l2.minimumInteritemSpacing = 2;
        l2.minimumLineSpacing = 5;
        _paletteCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(self.width/2, self.height/5, self.width/2, self.height*4/5) collectionViewLayout:l2];
        [_paletteCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"paletteCell"];
        _paletteCollectionView.backgroundColor = [UIColor clearColor];
        _paletteCollectionView.delegate = self;
        _paletteCollectionView.dataSource = self;
        [self.view addSubview:_paletteCollectionView];

        _toolsCollectionView.tag = COLLECTION_TOOL;
        _paletteCollectionView.tag = COLLECTION_PALETTE;

        [self formToolsArray];
    }
    return self;
}

- (void)formToolsArray
{
    if (!self.toolsArray) {
        self.toolsArray = [NSMutableArray new];
    }
    for (NSString* fileName in @[@"slice1", @"slice2", @"slice3", @"slice1", @"slice2", @"slice1", @"slice2", @"slice1", @"slice1", @"slice2", @"slice3", @"slice1", @"slice2", @"slice1", @"slice2", @"slice1", @"slice2", @"slice3", @"slice1", @"slice2", @"slice1", @"slice2", @"slice1", @"slice2", @"slice3", @"slice1", @"slice2", @"slice1", @"slice2"]) {
        UIImage *image = [UIImage imageNamed:[fileName stringByAppendingPathExtension:@"png"]];
        if (image) {
            UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
            imageView.backgroundColor = [UIColor cyanColor];
            [self.toolsArray addObject:imageView];
        }
    }
}



#pragma mark -
#pragma mark -
#pragma mark COLLECTION VIEW DELEGATES

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (collectionView.tag) {
        case COLLECTION_PALETTE: {
            return model.paletteUserColorsArray.count;
            }
            break;
        case COLLECTION_TOOL: {
            return self.toolsArray.count;
            }
            break;
        default: return 0;
            break;
        }
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (collectionView.tag) {
        case COLLECTION_PALETTE: {
            UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"paletteCell" forIndexPath:indexPath];
            cell.backgroundColor = model.paletteUserColorsArray[indexPath.row];
            return cell;
        }
            break;
        case COLLECTION_TOOL: {
            UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"toolCell" forIndexPath:indexPath];
            [cell addSubview:self.toolsArray[indexPath.row]];
            return cell;
        }
            break;
        default: return nil;
            break;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50, 50);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (collectionView.tag) {
        case COLLECTION_PALETTE: {
            UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
            model.selectedColor = cell.backgroundColor;
            self.colorView.backgroundColor = cell.backgroundColor;

        }
            break;
        case COLLECTION_TOOL: {
            //			UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
            switch (indexPath.row) {
                case 0:
                    //pen
                    model.drawingMode = DrawingModePen;
                    break;
                case 1:
                    model.drawingMode = DrawingModeFill;
//                    if (_fillMode) {
//                        _fillMode = NO;
//                        [self populateViewWithCells];
//                    } else {
//                        _pickMode = NO;
//                        _fillMode = YES;
//                        [self populateViewWithCells];
//                    }
                    break;
                case 2:
                    model.drawingMode = DrawingModeThickPen;
//                    self.selectedColor = model.selectedColor;
                    break;
                case 3:
                    //undo
                    model.pictureAreaData = model.pictureAreaDataUndoBuffer;
//                    [self.artView setNeedsDisplay];
//                    [self populateViewWithCells];
                    break;
                case 4:
                     model.drawingMode = DrawingModeColorPicker;
//                    if (_pickMode) {
//                        _pickMode = NO;
//                        [self populateViewWithCells];
//                    } else {
//                        _fillMode = NO;
//                        _pickMode = YES;
//                        [self populateViewWithCells];
//                    }
                    break;
                case 5:
                     model.drawingMode = DrawingModeEraser;
//                    if (![self.selectedColor isEqual:[UIColor clearColor]]) {
//                        model.selectedColor = self.selectedColor;
//                        self.selectedColor = [UIColor clearColor];
//                    }
                    break;
                case 6:
                    //magnify
//                    [self.view addSubview:self.zoomLevelButtonsView];
//                    self.zoomLevelButtonsView.hidden = NO;
                    break;
                case 7:
                    //moonlight
//                    if ([self.gridView.backgroundColor isEqual:[UIColor darkGrayColor]]) {
//                        self.gridView.backgroundColor = [UIColor lightGrayColor];
//                        self.artView.backgroundColor = [UIColor lightGrayColor];
//                    } else {
//                        self.gridView.backgroundColor = [UIColor darkGrayColor];
//                        self.artView.backgroundColor = [UIColor darkGrayColor];
//                    }
                    break;
                default:
                    break;
            }
            
            
        }
            break;
        default:
            break;
    }
    
    NSLog(@"scroll index %i", indexPath.row);
}


#pragma mark -
#pragma mark -
- (void)notified {
    _colorView.backgroundColor = model.selectedColor;
}
- (void)dealloc {
    [notificationCenter removeObserver:self];
}

@end
