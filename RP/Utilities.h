//
//  Utilities.h
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/25/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//



#import "NSMutableData+PixelData.h"
#import "UIImage+PixelData.h"
#import "UIColor+PixelData.h"
#import "UIView+ColorAtPoint.h"


@interface Utilities : NSObject
@end
