//
//  ProjectCell.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 10/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputField.h"


@interface ProjectCell : UICollectionViewCell

@property (strong, nonatomic) UILabel *filesCountLabel;
@property (strong, nonatomic) UIButton *deleteButton;

#warning rename button to inputField
@property (strong, nonatomic) InputField *inputField;



@end
