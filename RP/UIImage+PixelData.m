//
//  UIImage+PixelData.m
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/25/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "UIImage+PixelData.h"

@implementation UIImage (PixelData)

///Gets image from pixel data with size of image
+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size {
	if (size.width && size.height) {
		CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
		UIGraphicsBeginImageContext(rect.size);
		CGContextRef context = UIGraphicsGetCurrentContext();
		CGContextSetFillColorWithColor(context, [color CGColor]);
		CGContextFillRect(context, rect);
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		return image;
	} else return nil;
}


+ (UIImage *)imageWithData:(NSMutableData *)data forImage:(UIImage *)image {
	if (data && image) {
		CGImageRef imageRef = [image CGImage];
		NSUInteger width = CGImageGetWidth(imageRef);
		NSUInteger height = CGImageGetHeight(imageRef);
		
		unsigned char *rawData = [data mutableBytes];
		CGContextRef context;
		context = CGBitmapContextCreate(rawData, width, height, 8, 4*width, CGImageGetColorSpace(imageRef), kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
		imageRef = CGBitmapContextCreateImage(context);
		image = [UIImage imageWithCGImage:imageRef];
		CGContextRelease(context);
	}
	
	return image;
}






@end
