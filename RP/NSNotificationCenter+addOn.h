//
//  NSNotificationCenter+addOn.h
//  OneTrak
//
//  Created by Mikhail Baynov on 25/10/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import <Foundation/Foundation.h>




typedef NS_ENUM(NSInteger, Notifier) {
    NotifierReloadViews
};





@interface NSNotificationCenter (addOn)




- (void)postNotifier:(Notifier)notifier;

- (void)addObserver:(id)observer notifier:(Notifier)notifier;

+ (NSString *)stringFromNotifier:(Notifier)notifier;



@end
