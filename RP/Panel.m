//
//  Panel.m
//  RPAS
//
//  Created by Mikhail Baynov on 18/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "Panel.h"

@interface Panel () {
    Model *model;
}
@end



@implementation Panel

- (instancetype)init {
    if (self=[super init]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self=[super initWithCoder:aDecoder]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self=[super initWithFrame:frame]) {
        self.frame = frame;
        _view = [[UIView alloc]initWithFrame:frame];
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
    model = [Model sharedInstance];
    [self addSubview:_view];
    _viewModel = [[ViewModel alloc]init];
    _viewModel.view = _view;
    _rotationEnabled = NO;
    [self rotateToOrientation];
}




- (void)touchBegan:(CGPoint)point {
    [_viewModel touchBegan:point];
}
-(void)touchMoved:(CGPoint)point {
    [_viewModel touchMoved:point];
}
- (void)touchEnded:(CGPoint)point {
    [_viewModel touchEnded:point];
}



#pragma mark - SETTERS
- (void)setRotationEnabled:(BOOL)rotation {
    if (self.width == self.height) {
        _rotationEnabled = rotation;
    } else _rotationEnabled = NO;
}
- (void)setOrientation:(UIDeviceOrientation)orientation {
    if (!_rotationEnabled) {
        _orientation = orientation;
        [self rotateToOrientation];
    }
}
- (void)rotateToOrientation {
    CGFloat turn = 0;
    switch (_orientation) {
        case UIDeviceOrientationPortrait:
            turn = 1;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            turn = -1;
            break;
        case UIDeviceOrientationLandscapeRight:
            turn = 2;
        default:
            break;
    }
    _view.transform = CGAffineTransformMakeRotation(M_PI_2 * turn);
}
- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    _view.frame=self.bounds;
    [self rotateToOrientation];
}
- (void)setBackgroundColor:(UIColor *)backgroundColor {
    [super setBackgroundColor:backgroundColor];
    _view.backgroundColor = backgroundColor;
}
- (void)setView:(UIView *)view {
    if (!_view) {
        _view = [[UIView alloc]init];
    }
}
- (void)setViewModel:(ViewModel *)vm {
    _viewModel = vm;
    _viewModel.view = _view;
}






#pragma mark - GETTERS
- (UIColor *)backgroundColor {
    return _view.backgroundColor;
}
- (CGFloat)width {
    return self.frame.size.width;
}
- (CGFloat)height {
    return self.frame.size.height;
}



#pragma mark - OVERRIDE
- (CGPoint)convertPoint:(CGPoint)point fromView:(UIView *)view {
    point = [super convertPoint:point fromView:view];
    
    CGPoint p;
    switch (_orientation) {
        case UIDeviceOrientationPortrait:
            p = CGPointMake(point.y, self.frame.size.width-point.x);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            p = CGPointMake(self.frame.size.height-point.y, point.x);
            break;
        case UIDeviceOrientationLandscapeRight:
            p = CGPointMake(self.frame.size.width-point.x, self.height-point.y);
            break;
        case UIDeviceOrientationLandscapeLeft:
        default:
            p = point;
            break;
    }
    return p;
}
#pragma mark -




@end
