//
//  pixel_data.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 20/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#ifndef __ArtStudioPalettes__pixel_data__
#define __ArtStudioPalettes__pixel_data__


#include <vector>



struct Color
{
	unsigned char red, green, blue, alpha;
};
typedef std::vector<Color> Line;


class Pic
{
public:
	Color *data;
	size_t dsize;
	size_t width;
	size_t height;
	
	Pic() = default;
	Pic(size_t width, size_t height);
	Pic(size_t width, size_t height, Color* data);

	~Pic() = default;
};




//Pic();
//Pic(color* data,         size_t width, size_t height);


class CPPClass
{
public:
	CPPClass();
	Line palette(const Pic& pic);
	
	Pic& merge_image_ncolor_colors(Pic&, Color, Line);
	Pic& paste_picture_area(const Pic& pic1, Pic& pic, int x, int y);
	Pic& crop_add_pixels(Pic& pic, int lp, int rp, int tp, int bp);
	Pic& rotate_picture(Pic& pic);
	Pic& flip_picture(Pic& pic);
	Pic& interpolate2x(Pic& pic);
	Pic& downscale2x(Pic& pic);
	
	Pic& upscale2x(Pic& pic);
	Pic& upscale3x(Pic& pic);

};



#endif /* defined(__ArtStudioPalettes__pixel_data__) */
