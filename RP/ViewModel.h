//
//  ViewModel.h
//  RP
//
//  Created by Mikhail Baynov on 19/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "Panel.h"

@class Panel;

@interface ViewModel : NSObject

@property (nonatomic, strong) UIView *view;
@property (nonatomic, weak, readonly) Panel *panel;

@property (nonatomic, readonly) CGFloat width;
@property (nonatomic, readonly) CGFloat height;


- (instancetype)initWithPanel:(Panel *)panel;
- (void)touchBegan:(CGPoint)point;
- (void)touchMoved:(CGPoint)point;
- (void)touchEnded:(CGPoint)point;

@end
