//
//  ProjectNamePanel.h
//  RP
//
//  Created by Mikhail Baynov on 19/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "Panel.h"
#import "ViewModel.h"


@interface FileNameVM : ViewModel
<UICollectionViewDataSource,
UICollectionViewDelegate>


@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *filenamesArray;
@property (strong, nonatomic) NSMutableArray *iconsArray;


//@property (strong, nonatomic) NSString *filesDirectory;
//@property (strong, nonatomic) NSMutableString *projectName;


@end
