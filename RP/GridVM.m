//
//  GridVM.m
//  RP
//
//  Created by Mikhail Baynov on 23/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "GridVM.h"
#import "Model.h"
#import "NSNotificationCenter+addOn.h"


@interface GridVM ()
{
    Model *model;
    NSNotificationCenter *notificationCenter;
}
@property (nonatomic, strong) NSString *documentsDirectory;

@end

@implementation GridVM

- (instancetype)initWithPanel:(Panel *)panel {
    if (self = [super initWithPanel:panel]) {
        model = [Model sharedInstance];
        notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self notifier:NotifierReloadViews];
        _cellSize = 30;

        self.gridX = _cellSize;
        self.gridY = _cellSize;

        _insideX = model.pictureArea.size.width/2-(int)self.view.bounds.size.width/_cellSize/2;
        _insideY = model.pictureArea.size.height/2-(int)self.view.bounds.size.height/_cellSize/2;
        
        [self populateViewWithCells];
    }
    return self;
}

- (void)populateViewWithCells
{
    CGFloat xCoord = 0;
    CGFloat yCoord = 0;
    for (UIView *cell in self.view.subviews) {
        if ([cell isKindOfClass:[UIView class]]) {
            [cell removeFromSuperview];
        }
    }
    
    _insideX = 150;
    _insideY = 150;
    
    for (NSInteger y = 0; y < self.view.bounds.size.height; y = y + _cellSize) {
        for (NSInteger x = 0; x < self.view.bounds.size.width; x = x + _cellSize) {
            if (xCoord+_insideX < 0 || yCoord+_insideY < 0) {
//            } else if (xCoord+_insideX >= model.pictureArea.size.width || yCoord+_insideY >= model.pictureArea.size.height) {
            } else {
                UIView *cellView = [[UIView alloc]initWithFrame:CGRectMake(x, y, _cellSize, _cellSize)];
                cellView.layer.borderColor = [UIColor grayColor].CGColor;
                cellView.layer.borderWidth = .5;
                cellView.tag = 99;
                cellView.userInteractionEnabled = NO;
                
                cellView.backgroundColor = [UIColor colorFromPixelColorAtX:xCoord+_insideX Y:yCoord+_insideY ofData:model.pictureAreaData forImage:model.pictureArea];
                [self.view addSubview:cellView];
                
//                if (_pickMode || _fillMode) {
//                    cellView.layer.borderWidth = 0;
//                }
            }
            xCoord++;
        }
        self.xCount = (CGFloat)xCoord;
        xCoord = 0;
        yCoord++;
        self.yCount = (CGFloat)yCoord;
    }
//    self.zoomLevelButtonsView.hidden = YES;
//    [self.gridView addSubview:self.zoomLevelButtonsView];
}




#pragma mark -
#pragma mark TOUCHES
- (void)touchBegan:(CGPoint)point {
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
//    UIView *cell = [self.view hitTest:locationPoint withEvent:event];
    
    switch (model.drawingMode) {
        case DrawingModeColorPicker:
            [self colorSelected:[self.view colorAtPoint:point]];
            model.drawingMode = DrawingModePen;
            [self populateViewWithCells];
            break;
        case DrawingModeFill:
//            CGPoint locationPoint = [[touches anyObject] locationInView:self.gridView];
//            [self fillColorFromTouchAtPoint:locationPoint];
//            self.saveButton.hidden = NO;
            model.drawingMode = DrawingModePen;
//            [self.artView setNeedsDisplay];
            [self populateViewWithCells];
            break;
            
        case DrawingModePen:
        default:
            [self touchMoved:point];
            break;
    }
//    if (_pickMode) {
//    } else if (_fillMode) {
//    } else if ([cell isDescendantOfView:self.joystickView]) {
//        CGPoint insidePoint = [self.joystickView convertPoint:locationPoint fromView:self.view];
//        
//        
//        
//        _insideY += insidePoint.y / self.joystickView.frame.size.height * 8 - 4;
//        _insideX += insidePoint.x / self.joystickView.frame.size.width * 8 - 4;
//        [self populateViewWithCells];
//    } else {
//        [self touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];
//    }
//}

}

- (void)touchMoved:(CGPoint)point {
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
    UIView *cell = [self.view hitTest:point withEvent:nil];
//    if (cell.tag == 99 && !joystickOn) {
        [self dataChangeFromTouchAtPoint:point cell:cell];
//    } else if ([cell isDescendantOfView:self.artViewFrameView]) {
//        CGPoint insidePoint = [self.artView convertPoint:locationPoint fromView:self.view];
//        _insideX = insidePoint.x - _xCount;
//        _insideY = insidePoint.y - _yCount;
//        [self populateViewWithCells];
//    }
//    cell = nil;
//}
}


- (void)touchEnded:(CGPoint)point {
    [self populateViewWithCells];
    [notificationCenter postNotifier:NotifierReloadViews];
}

//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
//    UIView *cell = [self.view hitTest:locationPoint withEvent:event];
//    if (cell.tag == 99 && !joystickOn) {
//        [self dataChangeFromTouchAtPoint:[self.view convertPoint:locationPoint toView:self.gridView] cell:cell];
//    }
//    [self.artView setNeedsDisplay];
//    joystickOn = NO;
//    cell = nil;
//    
//    if (_thickPenMode) {
//        [self populateViewWithCells];
//    }
//}
//
//
- (void)colorSelected:(UIColor *)color {
    [self populateViewWithCells];
    model.selectedColor = color;
    //    self.colorView.backgroundColor = color;
}
//
//#pragma mark
//#pragma mark
//#pragma mark data change methods
//
- (void)dataChangeFromTouchAtPoint:(CGPoint)locationPoint cell:(UIView*)cell {
//    self.saveButton.hidden = NO;
//    if (self.selectedColorAlpha) {
        cell.backgroundColor = model.selectedColor;
//    } else {
//        cell.backgroundColor = [UIColor clearColor];
//    }
//    
//    if (self.thickPenMode) {
//        for (int i=-1; i<2; i++) {
//#warning CHANGES COLOR THE WRONG WAY ON CORNER CASES
//            model.pictureAreaData = [NSMutableData dataChangePixelColorRed:_selectedColorRed
//                                                                     Green:_selectedColorGreen
//                                                                      Blue:_selectedColorBlue
//                                                                     Alpha:_selectedColorAlpha
//                                                                      forX:(locationPoint.x)/_cellSize + _insideX -1
//                                                                         Y:(locationPoint.y)/_cellSize + _insideY +i
//                                                                    ofData:model.pictureAreaData
//                                                                  forImage:self.artView.image];
//            model.pictureAreaData = [NSMutableData dataChangePixelColorRed:_selectedColorRed
//                                                                     Green:_selectedColorGreen
//                                                                      Blue:_selectedColorBlue
//                                                                     Alpha:_selectedColorAlpha
//                                                                      forX:(locationPoint.x)/_cellSize + _insideX
//                                                                         Y:(locationPoint.y)/_cellSize + _insideY +i
//                                                                    ofData:model.pictureAreaData
//                                                                  forImage:self.artView.image];
//            model.pictureAreaData = [NSMutableData dataChangePixelColorRed:_selectedColorRed
//                                                                     Green:_selectedColorGreen
//                                                                      Blue:_selectedColorBlue
//                                                                     Alpha:_selectedColorAlpha
//                                                                      forX:(locationPoint.x)/_cellSize + _insideX +1
//                                                                         Y:(locationPoint.y)/_cellSize + _insideY +i
//                                                                    ofData:model.pictureAreaData
//                                                                  forImage:self.artView.image];
//        }
//    } else {
    
        model.pictureAreaData = [NSMutableData dataChangePixelColorRed:model.selectedColorRed
                                                                 Green:model.selectedColorGreen
                                                                  Blue:model.selectedColorBlue
                                                                 Alpha:model.selectedColorAlpha
                                                                  forX:(locationPoint.x )/_cellSize + _insideX
                                                                     Y:(locationPoint.y )/_cellSize + _insideY
                                                                ofData:model.pictureAreaData
                                                              forImage:model.pictureArea];
//    }
}
//
//
//- (void)fillColorFromTouchAtPoint:(CGPoint)locationPoint
//{
//    model.pictureAreaDataUndoBuffer = [NSMutableData dataFromImage:self.artView.image];
//    model.pictureAreaData = [NSMutableData dataContourFillWithColorRed:_selectedColorRed
//                                                                 Green:_selectedColorGreen
//                                                                  Blue:_selectedColorBlue
//                                                                 Alpha:_selectedColorAlpha
//                                                                 fromX:(locationPoint.x )/_cellSize + _insideX
//                                                                     Y:(locationPoint.y )/_cellSize + _insideY
//                                                                ofData:model.pictureAreaData
//                                                              forImage:self.artView.image];
//}
//
//
//#pragma mark
//#pragma mark
//#pragma mark  ACTIONS
//- (IBAction)saveButtonPressed:(id)sender
//{
//    model.pictureArea = self.artView.image;
//    model.picture = [model.cppManager pastePictureArea:model.pictureArea intoPicture:model.picture X:(int)model.pictureAreaRect.origin.x Y:(int)model.pictureAreaRect.origin.y];
//    
//    [model saveImage:model.picture toFile:model.fileNameFullPath];
//    self.saveButton.hidden = YES;
//}
//
//- (IBAction)backToPreview:(id)sender
//{
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//
//
//#pragma mark -
//#pragma mark -
//#pragma mark - SETTERS
//- (void)setSelectedColor:(UIColor *)selectedColor {
//    _selectedColor = selectedColor;
//    model.selectedColor = selectedColor;
//    CGFloat red;
//    CGFloat green;
//    CGFloat blue;
//    CGFloat alpha;
//    [_selectedColor getRed:&red green:&green blue:&blue alpha:&alpha];
//    _selectedColorRed = (int)(red * 255);
//    _selectedColorGreen = (int)(green * 255);
//    _selectedColorBlue = (int)(blue * 255);
//    _selectedColorAlpha = (int)(alpha * 255);
//}
//
//
//#pragma mark -
//#pragma mark -
//#pragma mark - colorSelected
//
//- (void)colorSelected:(UIColor *)color {
//    _pickMode = NO;
//    _fillMode = NO;
//    [self populateViewWithCells];
//    self.selectedColor = color;
//    self.colorView.backgroundColor = color;
//}
//

- (void)notified {
    [self populateViewWithCells];
}
- (void)dealloc
{
    [notificationCenter removeObserver:self];
}


@end