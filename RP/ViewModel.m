//
//  ViewModel.m
//  RP
//
//  Created by Mikhail Baynov on 19/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "ViewModel.h"

@implementation ViewModel


- (instancetype)initWithPanel:(Panel *)panel {
    if (self=[super init]) {
        _panel = panel;
        _view = [[UIView alloc]initWithFrame:panel.bounds];
        _width = _view.frame.size.width;
        _height = _view.frame.size.height;
        [_panel.view addSubview:_view];
    }
    return self;
}

- (void)touchBegan:(CGPoint)point {
    NSLog(@"touchBegan %f:%f %@", point.x, point.y, self.description);
}
-(void)touchMoved:(CGPoint)point {
    
}
- (void)touchEnded:(CGPoint)point {
    
}



@end
