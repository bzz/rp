//
//  ControlPanel.h
//  RP
//
//  Created by Mikhail Baynov on 18/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Panel.h"
#import "ControlButton.h"

@protocol ControlPanelDelegate <NSObject>

- (void)controlPanelButtonPressed:(ControlPanelButton)button;

@end



@interface ControlPanel : Panel

@property (nonatomic) ControlPanelButton mode;

@property (nonatomic, weak) id <ControlPanelDelegate> delegate;
@property (nonatomic, strong) NSMutableArray <ControlButton *> *buttonsArray;
@property (nonatomic, strong) ControlButton *cornerButton;


- (void)addButtonWithImage:(UIImage *)image selected:(UIImage *)imageSelected;
- (void)setCornerButtonImage:(UIImage *)image selected:(UIImage *)imageSelected;

- (void)rotateToOrientation;


@end
