//
//  Panel.h
//  RPAS
//
//  Created by Mikhail Baynov on 18/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewModel.h"
#import "Model.h"


@class ViewModel;

@interface Panel : UIView

@property (nonatomic, strong) ViewModel *viewModel;
@property (nonatomic, strong) UIView *view;

@property (nonatomic) BOOL rotationEnabled;
@property (nonatomic) UIDeviceOrientation orientation;
@property (nonatomic, readonly) CGFloat width;
@property (nonatomic, readonly) CGFloat height;



- (void)touchBegan:(CGPoint)point;
- (void)touchMoved:(CGPoint)point;
- (void)touchEnded:(CGPoint)point;


- (CGPoint)convertPoint:(CGPoint)point fromView:(UIView *)view;
- (void)rotateToOrientation;

@end
