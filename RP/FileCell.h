//
//  FileCell.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 11/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputField.h"


@interface FileCell : UICollectionViewCell <UITextFieldDelegate>


@property (strong, nonatomic) InputField *inputField;
@property (strong, nonatomic) UIImageView *pictureView;

@property (strong, nonatomic) UIButton *sendButton;
@property (strong, nonatomic) UIButton *makeCopyButton;
@property (strong, nonatomic) UIButton *deleteButton;





@end
