//
//  PreviewVM.h
//  RP
//
//  Created by Mikhail Baynov on 27/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "ViewModel.h"

@interface PreviewVM : ViewModel

@property (strong, nonatomic) UIImageView *pictureView;
@property (strong, nonatomic) UIView *areaView;



@end
