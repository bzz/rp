//
//  ProjectCell.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 10/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "ProjectCell.h"
#import "Model.h"
#import "NSNotificationCenter+addOn.h"


@interface ProjectCell () {
    Model *model;
    CGFloat width;
    CGFloat height;
}
@end


@implementation ProjectCell

- (void)prepareForReuse {
	[super prepareForReuse];
	self.deleteButton.hidden = NO;
}



- (id)init {
	if (self=[super init]) {
		[self setupInit];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ([super initWithCoder:aDecoder]) {
		[self setupInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    if ([super initWithFrame:frame]) {
		[self setupInit];
    }
    return self;
}

- (void)setupInit {
    model = [Model sharedInstance];
    width = self.frame.size.width;
    height = self.frame.size.height;

    
    self.backgroundColor = [UIColor hex:0x588FC5];
    

    _inputField = [[InputField alloc]initWithFrame:CGRectMake(0, 0, width, height/2)];
    [self addSubview:_inputField];
    
    self.filesCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(width/5, height/2, width, height/2)];
    [self addSubview:self.filesCountLabel];
    
    _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _deleteButton.frame = CGRectMake(0, height/2, width/5, height/2);
    _deleteButton.backgroundColor = [UIColor magentaColor];
    [_deleteButton addTarget:self action:@selector(deleteButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_deleteButton];
    
//    _inputField.backgroundColor = [UIColor darkGrayColor];
//    self.filesCountLabel.backgroundColor = [UIColor purpleColor];
    //@property (weak, nonatomic) UIButton *deleteButton;
    
}

- (void)deleteButtonPressed {
    if (_deleteButton.selected) {
        _deleteButton.selected = NO;
        [model.fileManager removeItemAtPath:[model.documentsDirectory stringByAppendingPathComponent:_inputField.label.text] error:nil];
        [[NSNotificationCenter defaultCenter]performSelector:@selector(postNotifier:) withObject:NotifierReloadViews afterDelay:1.0];
//        [[NSNotificationCenter defaultCenter]postNotifier:NotifierProjectSelected];
    } else {
        _deleteButton.backgroundColor = [_deleteButton.backgroundColor inverseColor];
        _deleteButton.selected = YES;
        [_deleteButton performSelector:@selector(setBackgroundColor:) withObject:[_deleteButton.backgroundColor inverseColor] afterDelay:1.0];
        [_deleteButton performSelector:@selector(setSelected:) withObject:@"NO" afterDelay:1.0];
    }
}


@end
