//
//  NSNotificationCenter+addOn.m
//  OneTrak
//
//  Created by Mikhail Baynov on 25/10/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import "NSNotificationCenter+addOn.h"

@implementation NSNotificationCenter (addOn)


- (void)postNotifier:(Notifier)notifier
{
    NSLog(@"%@", [NSNotificationCenter stringFromNotifier:notifier]);
    [self postNotificationName:[NSNotificationCenter stringFromNotifier:notifier] object:nil];
}


- (void)addObserver:(id)observer notifier:(Notifier)notifier
{
    [self addObserver:observer selector:@selector(notified) name:[NSNotificationCenter stringFromNotifier:notifier] object:nil];
}

+ (NSString*)stringFromNotifier:(Notifier)notifier
{
    switch (notifier) {
        case NotifierReloadViews:
            return @"NotifierProjectSelected";
            break;
        default:
            break;
    }
}



@end
