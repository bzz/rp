//
//  ViewController.h
//  RP
//
//  Created by Mikhail Baynov on 18/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Panel.h"
#import "ControlPanel.h"
#import "NSNotificationCenter+addOn.h"





@interface ViewController : UIViewController <ControlPanelDelegate>


@property (strong, nonatomic) Panel *panel1;
@property (strong, nonatomic) Panel *panel2;
@property (strong, nonatomic) Panel *panel3;
@property (strong, nonatomic) ControlPanel *controlPanel;



@end
