//
//  ViewController.m
//  RP
//
//  Created by Mikhail Baynov on 18/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "ViewController.h"
#import "ProjectNameVM.h"
#import "Model.h"

#import "ToolsVM.h"
#import "ArtPreviewVM.h"
#import "GridVM.h"

#import "ProjectNameVM.h"
#import "FileNameVM.h"
#import "PreviewVM.h"




@interface ViewController () {
    CGFloat landscapeScreenWidth;
    CGFloat landscapeScreenHeight;
    CGRect panel1Frame;
    CGRect panel2Frame;
    CGRect panel3Frame;
    CGRect controlPanelFrame;
    
    Model *model;
    NSNotificationCenter *notificationCenter;
}
@end



@implementation ViewController
- (BOOL)prefersStatusBarHidden {
    return YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    model = [Model sharedInstance];
    notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self notifier:NotifierReloadViews];
    
    landscapeScreenWidth = 1024;
    landscapeScreenHeight = 768;
    [self setupPanels];
    

    //tmp
    model.selectedColor = [UIColor magentaColor];
    model.paletteUserColorsArray = [NSMutableArray arrayWithObjects:[UIColor redColor], [UIColor orangeColor], [UIColor blackColor], [UIColor grayColor], [UIColor greenColor], [UIColor purpleColor], [UIColor yellowColor], [UIColor redColor], [UIColor orangeColor], [UIColor blackColor], [UIColor grayColor], [UIColor greenColor], [UIColor purpleColor], [UIColor yellowColor], [UIColor redColor], [UIColor orangeColor], [UIColor blackColor], [UIColor grayColor], [UIColor greenColor], [UIColor purpleColor], [UIColor yellowColor], [UIColor redColor], [UIColor orangeColor], [UIColor blackColor], [UIColor grayColor], [UIColor greenColor], [UIColor purpleColor], [UIColor yellowColor], [UIColor redColor], [UIColor orangeColor], [UIColor blackColor], [UIColor grayColor], [UIColor greenColor], [UIColor purpleColor], [UIColor yellowColor], nil];

    model.picture = [UIImage imageNamed:@"pig"];
    model.pictureArea = [[UIImage alloc]init];
    model.pictureArea = model.picture;
    model.pictureAreaData = [NSMutableData dataFromImage:model.pictureArea];
    model.pictureAreaDataUndoBuffer = model.pictureAreaData;
    
    
    
    [_controlPanel addButtonWithImage:[UIImage imageNamed:@"file"] selected:[UIImage imageNamed:@"fileSelected"]];
    [_controlPanel addButtonWithImage:[UIImage imageNamed:@"edit"] selected:[UIImage imageNamed:@"editSelected"]];
    [_controlPanel addButtonWithImage:[UIImage imageNamed:@"trans"] selected:[UIImage imageNamed:@"transSelected"]];
    [_controlPanel addButtonWithImage:[UIImage imageNamed:@"palette"] selected:[UIImage imageNamed:@"paletteSelected"]];
//    [_controlPanel setCornerButtonImage:[UIImage imageNamed:@"pig"] selected:[UIImage imageNamed:@"pig"]];
    [self controlPanelButtonPressed:ControlPanelFile];
}



- (void)setupPanels {
    panel1Frame = CGRectMake(1, 88, 340, 340);
    panel2Frame = CGRectMake(1, 428, 340, 340);
    panel3Frame = CGRectMake(343, 88, 680, 680);
    controlPanelFrame = CGRectMake(1, 1, 1022, 85);
    
    
    _panel1 = [[Panel alloc]init];
    _panel2 = [[Panel alloc]init];
    _panel3 = [[Panel alloc]init];

    _controlPanel = [[ControlPanel alloc]init];
    _controlPanel.delegate = self;
    _controlPanel.mode = ControlPanelSave;  //99 by default
    
    
    _panel1.rotationEnabled = YES;
    _panel2.rotationEnabled = YES;
    _panel3.rotationEnabled = YES;
    
    [self layoutPanels];
    
    [self.view addSubview:_panel1];
    [self.view addSubview:_panel2];
    [self.view addSubview:_panel3];
    [self.view addSubview:_controlPanel];
}


#pragma mark - LAYOUT MAGIC
- (void)layoutPanels {
    UIDeviceOrientation orientation = model.orientation;
    _panel1.frame = [self rectForOrientation:orientation landScapeLeftFrame:panel1Frame];
    _panel2.frame = [self rectForOrientation:orientation landScapeLeftFrame:panel2Frame];
    _panel3.frame = [self rectForOrientation:orientation landScapeLeftFrame:panel3Frame];
    _controlPanel.frame = [self rectForOrientation:orientation landScapeLeftFrame:controlPanelFrame];
}

- (CGRect)rectForOrientation:(UIDeviceOrientation)orientation landScapeLeftFrame:(CGRect)frame {
    CGRect rect;
    switch (orientation) {
        case UIDeviceOrientationPortrait:
            rect = CGRectMake(landscapeScreenHeight-frame.size.height-frame.origin.y, frame.origin.x, frame.size.height, frame.size.width);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            rect = CGRectMake(frame.origin.y, landscapeScreenWidth-frame.size.width-frame.origin.x, frame.size.height, frame.size.width);
            break;
        case UIDeviceOrientationLandscapeRight:
            rect = CGRectMake(landscapeScreenWidth-frame.size.width-frame.origin.x, landscapeScreenHeight-frame.size.height-frame.origin.y, frame.size.width, frame.size.height);
            break;
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationUnknown:
        default:
            rect = frame;
            break;
    }
    return rect;
}





#pragma mark - ROTATION
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [self layoutPanels];
    [UIView setAnimationsEnabled:NO];
    
    [_controlPanel rotateToOrientation];
}




#pragma mark - TOUCHES
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    CGPoint point = [touches.anyObject locationInView:self.view];
    Panel *panel = [self hitTest:point];
    [panel touchBegan:[panel convertPoint:point fromView:self.view]];
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    CGPoint point = [touches.anyObject locationInView:self.view];
    Panel *panel = [self hitTest:point];
    [panel touchMoved:[panel convertPoint:point fromView:self.view]];
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    CGPoint point = [touches.anyObject locationInView:self.view];
    Panel *panel = [self hitTest:point];
    [panel touchEnded:[panel convertPoint:point fromView:self.view]];
}
- (Panel *)hitTest:(CGPoint)point {
    Panel *panel = [[Panel alloc]init];
    if (CGRectContainsPoint(_controlPanel.frame, point)) {
        panel = _controlPanel;
    } else if (CGRectContainsPoint(_panel1.frame, point)) {
        panel = _panel1;
    } else if (CGRectContainsPoint(_panel2.frame, point)) {
        panel = _panel2;
    } else if (CGRectContainsPoint(_panel3.frame, point)) {
        panel = _panel3;
    }
    panel.orientation = model.orientation;
    return panel;
}
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    [self touchesEnded:touches withEvent:event];
}



#pragma mark ControlPanelDelegate
#pragma mark -
- (void)controlPanelButtonPressed:(ControlPanelButton)button {
#warning UPSIDE DOWN PANEL1 and PANEL2 SCROLLVIEWS DONT SCROLL
    
    
    NSLog(@"controlPanelButtonPressed %i", button);
    if (button != _controlPanel.mode &&
        button != ControlPanelSave) {
        for (UIView *view in _panel1.view.subviews) {
            [view removeFromSuperview];
        }
        for (UIView *view in _panel2.view.subviews) {
            [view removeFromSuperview];
        }
        for (UIView *view in _panel3.view.subviews) {
            [view removeFromSuperview];
        }

        

        switch (button) {
            case ControlPanelFile:
                _panel1.viewModel = [[ProjectNameVM alloc]initWithPanel:_panel1];
                _panel2.viewModel = [[FileNameVM alloc]initWithPanel:_panel2];
                _panel3.viewModel = [[PreviewVM alloc]initWithPanel:_panel3];
                
                break;
            case ControlPanelEdit:
                _panel1.viewModel = [[ToolsVM alloc]initWithPanel:_panel1];
                _panel2.viewModel = [[ArtPreviewVM alloc]initWithPanel:_panel2];
                _panel3.viewModel = [[GridVM alloc]initWithPanel:_panel3];
                break;
                
            default:
                break;
        }
    }
}

- (void)notified {
    
}

- (void)dealloc
{
    [notificationCenter removeObserver:self];
}

@end
