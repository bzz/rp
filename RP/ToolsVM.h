//
//  ToolsPanel.h
//  RP
//
//  Created by Mikhail Baynov on 19/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "Panel.h"
#import "ViewModel.h"

@interface ToolsVM : ViewModel
<UICollectionViewDataSource,
UICollectionViewDelegate>


@property (nonatomic, strong) UIView *colorView;
@property (nonatomic, strong) UICollectionView *toolsCollectionView;
@property (nonatomic, strong) UICollectionView *paletteCollectionView;
@property (nonatomic, strong) NSMutableArray *toolsArray;


@end
