//
//  AView.m
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/19/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "ArtPreviewVM.h"
#import "Model.h"

@import CoreGraphics;
@import QuartzCore;

#import "NSNotificationCenter+addOn.h"


@interface ArtPreviewVM ()
{
	Model *model;
    NSNotificationCenter *notificationCenter;
}
@property (nonatomic, strong) NSString *documentsDirectory;

@end

@implementation ArtPreviewVM

- (instancetype)initWithPanel:(Panel *)panel {
    if (self = [super initWithPanel:panel]) {
        model = [Model sharedInstance];
        notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self notifier:NotifierReloadViews];
        _artView = [[ArtView alloc]initWithFrame:panel.bounds];
        
//        _artView.image = model.pictureArea;
//        model.pictureAreaData = [NSMutableData dataFromImage:_artView.image];
//        model.pictureAreaDataUndoBuffer = model.pictureAreaData;

        
        
        _artView.image = model.pictureArea;
        [self.view addSubview:_artView];
    }
    return self;
}

- (void)notified {
    [self.artView setNeedsDisplay];
}
- (void)dealloc {
    [notificationCenter removeObserver:self];
}



@end
