//
//  InputField.h
//  RP
//
//  Created by Mikhail Baynov on 27/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputField : UIView <UITextFieldDelegate>

@property (strong, nonatomic, readwrite) UIView *inputAccessoryView;
@property (strong, nonatomic) UITextField *textField;

@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UIView *button;



@end
