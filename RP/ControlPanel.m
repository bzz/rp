//
//  ControlPanel.m
//  RP
//
//  Created by Mikhail Baynov on 18/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "ControlPanel.h"
#import "NSNotificationCenter+addOn.h"


@interface ControlPanel () {
    Model *model;
    NSNotificationCenter *notificationCenter;
}

@end



@implementation ControlPanel


- (instancetype)init {
    if (self=[super init]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self=[super initWithCoder:aDecoder]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self=[super initWithFrame:frame]) {
        self.frame = frame;
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
    model = [Model sharedInstance];
    notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self notifier:NotifierReloadViews];
    _buttonsArray = [NSMutableArray new];
    _cornerButton = [ControlButton new];
}



- (void)touchBegan:(CGPoint)point {
    [super touchBegan:point];
    for (int i=0; i<self.buttonsArray.count; ++i) {
        self.buttonsArray[i].selected = NO;
        if (CGRectContainsPoint(self.buttonsArray[i].frame, [self touchRealPoint:point])) {
            self.buttonsArray[i].selected = YES;
            [self.delegate controlPanelButtonPressed:i];
            self.mode = i;
        }
    }
    
    if (CGRectContainsPoint(self.cornerButton.frame, [self touchRealPoint:point])) {
        [self.delegate controlPanelButtonPressed:ControlPanelSave];
    }
}
- (CGPoint)touchRealPoint:(CGPoint)point {
    CGPoint p;
    switch (model.orientation) {
        case UIDeviceOrientationPortrait:
            p = CGPointMake(point.y, point.x);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            p = CGPointMake(point.y, self.width-point.x);
            break;
        case UIDeviceOrientationLandscapeRight:
            p = CGPointMake(self.width-point.x, point.y);
            break;
        case UIDeviceOrientationLandscapeLeft:
        default:
            p = point;
            break;
    }
    return p;
}







#pragma mark - GETTERS
- (CGFloat)width {
    return MAX(self.frame.size.width, self.frame.size.height);
}
- (CGFloat)height {
    return MIN(self.frame.size.width, self.frame.size.height);
}
#pragma mark - SETTERS





- (void)rotateToOrientation {
    for (int i=0; i<self.buttonsArray.count; ++i) {
        CGPoint point = CGPointMake(i*self.height, 0);
        CGPoint p = [self buttonRealPoint:point];
        self.buttonsArray[i].frame = CGRectMake(p.x, p.y, self.height, self.height);
        [self addSubview:self.buttonsArray[i]];
    }
    CGPoint p = [self buttonRealPoint:CGPointMake(self.width - self.height, 0)];
    self.cornerButton.frame = CGRectMake(p.x, p.y, self.height, self.height);
    [self addSubview:self.cornerButton];
}


- (CGPoint)buttonRealPoint:(CGPoint)point {
    CGPoint p;
    switch (model.orientation) {
        case UIDeviceOrientationPortrait:
            p = CGPointMake(point.y, point.x);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            p = CGPointMake(point.y, self.width-self.height-point.x);
            break;
        case UIDeviceOrientationLandscapeRight:
            p = CGPointMake(self.width-self.height-point.x, point.y);
            break;
        case UIDeviceOrientationLandscapeLeft:
        default:
            p = point;
            break;
    }
    return p;
}

- (void)addButtonWithImage:(UIImage *)image selected:(UIImage *)imageSelected {
    ControlButton *b = [[ControlButton alloc]init];
    CGPoint p = [self buttonRealPoint:CGPointMake(_buttonsArray.count*self.height, 0)];
    b.frame = CGRectMake(p.x, p.y, self.height, self.height);
    
    b.image = image;
    b.imageSelected = imageSelected;
    [self.buttonsArray addObject:b];
    [self addSubview:b];
}

- (void)setCornerButtonImage:(UIImage *)image selected:(UIImage *)imageSelected {
    _cornerButton.image = image;
    _cornerButton.imageSelected = imageSelected;
}


#pragma mark -
#pragma mark -
- (void)notified {
#warning SAVE IMAGE TO FILE
}
- (void)dealloc {
    [notificationCenter removeObserver:self];
}


@end
