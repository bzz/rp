//
//  InputField.m
//  RP
//
//  Created by Mikhail Baynov on 27/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "InputField.h"
#import "Model.h"
#import "NSNotificationCenter+addOn.h"

@interface InputField () {
    Model *model;
}

@end

@implementation InputField

- (instancetype)init {
    if (self=[super init]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self=[super initWithCoder:aDecoder]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self=[super initWithFrame:frame]) {
        self.frame = frame;
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
    model = [Model sharedInstance];
    
    _textField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 260, 44)];
    _textField.backgroundColor = [UIColor orangeColor];
    _textField.delegate = self;
    UIBarButtonItem *textFieldItem = [[UIBarButtonItem alloc] initWithCustomView:self.textField];
    UIToolbar* toolBar = [[UIToolbar alloc] init];
    toolBar.items = @[textFieldItem];
    [toolBar sizeToFit];
    self.inputAccessoryView = toolBar;

    
    _label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width*4/5, self.frame.size.height)];
    _label.userInteractionEnabled = NO;
    [self addSubview:_label];
    
    _button = [[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width*4/5, 0, self.frame.size.width/5, self.frame.size.height)];
    _button.backgroundColor = [UIColor redColor];
    [self addSubview:_button];
}



- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)renameButtonTapped {
    [_button becomeFirstResponder];
    [self.textField becomeFirstResponder];
}




- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if (CGRectContainsPoint(_button.frame, point)) {
        [self becomeFirstResponder];
        [self.textField becomeFirstResponder];
        return YES;
    } else return NO;
}

//////
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if ([_label.text.pathExtension isEqual:@"png"]) {
        _textField.text = [_label.text stringByDeletingPathExtension];
    } else _textField.text = _label.text;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self resignFirstResponder];
    if ([_label.text.pathExtension isEqual:@"png"]) {
        [self renameDir:_label.text toDir:[_textField.text stringByAppendingPathExtension:@"png"]];
        _label.text = [_textField.text stringByAppendingPathExtension:@"png"];
    } else {
        [self renameDir:_label.text toDir:_textField.text];
        _label.text = _textField.text;
    }
    return YES;
}


#pragma mark -
#pragma mark -
#pragma mark - UITextFieldDelegate

- (void)renameDir:(NSString*)fromDir toDir:(NSString*)toDir {
    NSError *error;
    if ([_label.text.pathExtension isEqual:@"png"]) {
    [[NSFileManager defaultManager] moveItemAtPath:[model.filesDirectory stringByAppendingPathComponent:fromDir]
                                            toPath:[model.filesDirectory stringByAppendingPathComponent:toDir] error:&error];
    } else {
        [[NSFileManager defaultManager] moveItemAtPath:[model.documentsDirectory stringByAppendingPathComponent:fromDir]
                                                toPath:[model.documentsDirectory stringByAppendingPathComponent:toDir] error:&error];
    }
    
    if (!error) {
        [[NSNotificationCenter defaultCenter] postNotifier:NotifierReloadViews];
    }
}

#pragma mark
#pragma mark


@end
