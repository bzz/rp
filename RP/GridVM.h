//
//  GridVM.h
//  RP
//
//  Created by Mikhail Baynov on 23/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModel.h"

@interface GridVM : ViewModel

@property (nonatomic) NSInteger cellSize;
@property (nonatomic) NSInteger gridX;
@property (nonatomic) NSInteger gridY;

@property (nonatomic) NSInteger insideX;
@property (nonatomic) NSInteger insideY;


@property (nonatomic) CGFloat xCount;
@property (nonatomic) CGFloat yCount;



@end
