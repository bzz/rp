//
//  ProjectNamePanel.m
//  RP
//
//  Created by Mikhail Baynov on 19/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "FileNameVM.h"
#import "Model.h"
#import "FileCell.h"
#import "NSNotificationCenter+addOn.h"

@interface FileNameVM ()
{
    Model *model;
    NSNotificationCenter *notificationCenter;
    UIImage *emptyImage;
}
@end


@implementation FileNameVM
- (instancetype)init {
    NSLog(@"USE INIT WITH PANEL, NOT INIT!");
    return nil;
}


- (instancetype)initWithPanel:(Panel *)panel {
    if (self = [super initWithPanel:panel]) {
        model = [Model sharedInstance];
        notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self notifier:NotifierReloadViews];
        
        emptyImage = [UIImage imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(40, 40)];  //testtest
        
//        _button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_button addTarget:self action:@selector(newProjectButtonPressed) forControlEvents:UIControlEventTouchUpInside];
//        [_button setTitle:@"New project" forState:UIControlStateNormal];
//        _button.frame = CGRectMake(0, 0, self.width, self.height/5);
//        _button.backgroundColor = [UIColor darkGrayColor];
//        [self.view addSubview:_button];
        
        UICollectionViewFlowLayout *l=[[UICollectionViewFlowLayout alloc] init];
        l.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
        l.minimumInteritemSpacing = 2;
        l.minimumLineSpacing = 5;
        l.itemSize = CGSizeMake(self.width - 10, (self.width-10)*3/5);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, self.height/5, self.width, self.height*4/5) collectionViewLayout:l];
        
        
        [_collectionView registerClass:[FileCell class] forCellWithReuseIdentifier:@"Cell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_collectionView];
        
        
        [self formFilenamesArray];
        
        
    }
    return self;
}

- (void)saveImage:(UIImage*)image toFile:(NSString *)fileName {
    if (![[NSFileManager defaultManager] fileExistsAtPath:model.filesDirectory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:model.filesDirectory withIntermediateDirectories:NO attributes:nil error:nil];
    }
    [UIImagePNGRepresentation(image) writeToFile:[model.filesDirectory  stringByAppendingPathComponent:fileName] atomically:YES];
}

- (void)formFilenamesArray {
    if (self.filenamesArray) {
        [self.filenamesArray removeAllObjects];
        [self.iconsArray removeAllObjects];
    } else 	{
        self.filenamesArray = [NSMutableArray new];
        self.iconsArray = [NSMutableArray new];
    }
    NSArray *array = [model.fileManager contentsOfDirectoryAtPath:model.filesDirectory error:nil];
    if (array.count) {
        for (NSString *file in array) {
            if ([file.pathExtension isEqual:@"png"]) {
                BOOL isDirectory = NO;
                if ([model.fileManager fileExistsAtPath:[model.filesDirectory stringByAppendingPathComponent:file] isDirectory:&isDirectory]) {
                    UIImage *tmp = [UIImage imageWithContentsOfFile:[model.filesDirectory stringByAppendingPathComponent:file]];
                    if (tmp) {
                        [self.filenamesArray addObject:file];
                        [self.iconsArray addObject:tmp];
                    }
                }
            }
        }
    }
    array = nil;
}




#pragma mark
#pragma mark Delegates
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *collectionViewCellIdentifier = @"Cell";
    
    FileCell *cell = (FileCell *)[collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellIdentifier forIndexPath:indexPath];
    cell.inputField.label.text = self.filenamesArray[indexPath.row];
    cell.pictureView.image = self.iconsArray[indexPath.row];

    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.filenamesArray.count;
}

- (NSInteger)countFilesAtPath:(NSString*)path {
    NSArray *array = [model.fileManager contentsOfDirectoryAtPath:path error:nil];
    NSInteger count = array.count;
    for (path in array) {
        BOOL isDirectory = NO;
        if ([model.fileManager fileExistsAtPath:[model.documentsDirectory stringByAppendingPathComponent:path] isDirectory:&isDirectory]) {
            if (isDirectory) {
                count--;
            }
        }
    }
    return count;
}



#pragma mark
#pragma mark ACTIONS

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    FileCell *cell = (FileCell*)[collectionView cellForItemAtIndexPath:indexPath];
    model.fileNameFullPath = [model.filesDirectory stringByAppendingPathComponent:cell.inputField.label.text];
    [[NSNotificationCenter defaultCenter] postNotifier:NotifierReloadViews];
    [self.collectionView reloadData];
}






- (void)notified {
    [self.collectionView reloadData];
    if (![model.fileManager contentsOfDirectoryAtPath:model.filesDirectory error:nil].count) {
        if (model.picture) {
            [self saveImage:model.picture toFile:@"untitled.png"];
        } else {
            [self saveImage:[UIImage imageNamed:@"blank.png"] toFile:@"untitled.png"];
        }
    }
    [self formFilenamesArray];
}
- (void)dealloc
{
    [notificationCenter removeObserver:self];
}


@end
