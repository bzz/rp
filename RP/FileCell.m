//
//  ProjectCell.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 10/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "FileCell.h"

#import "Model.h"


@interface FileCell () {
    CGFloat width;
    CGFloat height;
}

@end


@implementation FileCell

- (void)prepareForReuse {
    [super prepareForReuse];
    self.deleteButton.hidden = NO;
}



- (id)init {
    if (self=[super init]) {
        [self setupInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ([super initWithCoder:aDecoder]) {
        [self setupInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    if ([super initWithFrame:frame]) {
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
    width = self.frame.size.width;
    height = self.frame.size.height;
    
    
    self.backgroundColor = [UIColor brownColor];
    
    
    _inputField = [[InputField alloc]initWithFrame:CGRectMake(0, 0, width, height/5)];
    _inputField.backgroundColor = [UIColor greenColor];
    [self addSubview:_inputField];
    
    _pictureView = [[UIImageView alloc]initWithFrame:CGRectMake(0, height/5, width*4/5, height*4/5)];
    _pictureView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_pictureView];
}


@end
